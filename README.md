# LRU Cache

This data structure works like a key-value map with a fixed capacity.

When it's full and a new element needs to be added, the least recently used element is replaced.

This implementation's asymptotic time complexity is constant in _capacity_ for `add`, `get` and `remove`.

## Usage

To use the cache in a cpp project, just include the `LRUCache.h` file in your project.

To start the tests:

```
g++ -o main main.cpp && ./main
```

## Benchmark

### Theory

Let $`C`$ be the capacity.

* `constructor(uint)` runs in $`O(1)`$.
* `.add(TKey, TValue)` runs in $`O(1)`$.
* `.get(TKey)` runs in $`O(1)`$.
* `.remove(TKey)` runs in $`O(1)`$.

### Results

These results were obtained using `benchmark.cpp`.

![Performance](plot.png)

| Capacity |  GET |  ADD |
|---------:|-----:|-----:|
|      500 | 4048 | 5442 |
|      750 | 4365 | 5468 |
|     1000 | 4021 | 5760 |
|     3000 | 4406 | 5408 |
|     5000 | 4072 | 5707 |
|     7500 | 4434 | 5729 |
|    10000 | 4083 | 5261 |
|    30000 | 4456 | 5570 |
|    50000 | 4158 | 5330 |
|    75000 | 4315 | 5394 |
|   100000 | 4246 | 5498 |
