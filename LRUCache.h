/**
 * LRU Cache
 * Efficient implementation of an array that replaces the least recently used
 * item when it's full.
 *
 * @author https://gitlab.com/g-dv
 * @date Sept. 2020
 */

#include <exception>
#include <list>
#include <unordered_map>
#include <utility>

using namespace std;

template <class TKey, class TValue> class LRUCache {
public:
  /**
   * Constructor
   *
   * O(1)
   *
   * @param capacity Maximum number of elements in the cache
   */
  LRUCache(uint capacity) : _capacity(capacity) {
    if (!capacity) {
      throw invalid_argument("The capacity cannot be null.");
    }
  }

  /**
   * Adds an element.
   *
   * Adds the pair <key, value> in the map. Adds the key to the FIFO. Stores
   * the address of the key's node in the FIFO in order to know where it is
   * so that it can be erased quickly if needed.
   * If the map is full, remove the least recently used element. (ie. the one
   * in first position of the FIFO.)
   * If the key exists already, overwrite it.
   *
   * O(log(n))
   *
   * @param key Element's identifier
   * @param value Element value
   */
  void add(TKey key, TValue value) {
    // remove the element if the key exists already
    if (_map.find(key) != _map.end()) {
      // use the list iterator stored in the map to erase the key from the FIFO
      _queue.erase(_map.at(key).second);
      // remove the element from the map
      _map.erase(key);
    }
    // remove the LRU element if the cache is full
    if (_map.size() >= _capacity) {
      // remove element which is in front of the FIFO
      _map.erase(_queue.front());
      // remove oldest key from the FIFO
      _queue.pop_front();
    }
    // add the key in the FIFO
    _queue.push_back(key);
    // store the value and the address of the FIFO's node in the map
    _map.insert(make_pair(key, make_pair(value, --_queue.end())));
  }

  /**
   * Finds an element.
   *
   * Returns the value corresponding to the key. If the element does not exist
   * in the map, throws an exception. If the element exists, update its "last
   * used" status by moving its key back to the end of the FIFO.
   *
   * O(log(n))
   *
   * @param key Element to retrieve
   * @return value corresponding to the key
   */
  TValue get(TKey key) {
    if (_map.find(key) == _map.end()) {
      throw out_of_range("The key does not exist in the cache.");
    }
    // use the list iterator stored in the map to remove the key from the FIFO
    _queue.erase(_map.at(key).second);
    // add the key at the end of the FIFO
    _queue.push_back(key);
    // update the list iterator stored
    _map.at(key).second = --_queue.end();
    // retrieve the value
    return _map.at(key).first;
  }

  /**
   * Removes an element.
   *
   * O(log(n))
   *
   * @param key Element to remove
   */
  void remove(TKey key) {
    if (_map.find(key) == _map.end()) {
      throw out_of_range("The key does not exist in the cache.");
    }
    // use the list iterator stored in the map to remove the key from the FIFO
    _queue.erase(_map.at(key).second);
    // remove the element from the map
    _map.erase(key);
  }

private:
  uint _capacity;
  unordered_map<TKey, pair<TValue, typename list<TKey>::iterator>> _map;
  list<TKey> _queue;
};
