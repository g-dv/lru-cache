#include "LRUCache.h"
#include <chrono>
#include <iostream>

using namespace std;
using namespace chrono;

int main() {
  uint NB_TESTS = 11;
  uint NB_OPERATIONS_PER_TEST = 10000000;
  uint CAPACITIES[NB_TESTS] = {500,   750,   1000,  3000,  5000,  7500,
                               10000, 30000, 50000, 75000, 100000};

  // get operation
  cout << "GET" << endl;
  cout << "capacity time (ms)" << endl;
  for (uint i = 0; i < NB_TESTS; i++) {
    // init the cache - O(1)
    LRUCache<int, int> cache(CAPACITIES[i]);
    for (uint j = 0; j < CAPACITIES[i]; j++) {
      cache.add(j, 1);
    }
    // measure - O(NB_OP log(CAPACITY))
    auto begin = steady_clock::now();
    for (uint j = 0; j < NB_OPERATIONS_PER_TEST; j++) {
      cache.get(j % CAPACITIES[i]);
    }
    auto end = steady_clock::now();
    // print
    auto duration = duration_cast<milliseconds>(end - begin).count();
    cout << CAPACITIES[i] << "\t " << duration << endl;
  }

  // add operation
  cout << endl << "ADD" << endl;
  cout << "capacity time (ms)" << endl;
  for (uint i = 0; i < NB_TESTS; i++) {
    // init the cache - O(1)
    LRUCache<int, int> cache(CAPACITIES[i]);
    // measure - O(NB_OP log(CAPACITY))
    auto begin = steady_clock::now();
    for (uint j = 0; j < NB_OPERATIONS_PER_TEST; j++) {
      cache.add(j, 1);
    }
    auto end = steady_clock::now();
    // print
    auto duration = duration_cast<milliseconds>(end - begin).count();
    cout << CAPACITIES[i] << "\t " << duration << endl;
  }

  return 0;
}
