#include "LRUCache.h"
#include <exception>
#include <iostream>
#include <string>

using namespace std;

int main() {
  // Init.
  LRUCache<string, int> cache(2);

  // Test 1
  // Should throw an invalid_argument exception when capacity=0
  try {
    LRUCache<string, int>(0);
    cout << "[KO]";
  } catch (invalid_argument) {
    cout << "[OK]";
  }
  cout << " Should throw an invalid_argument exception when capacity=0" << endl;

  // Test 2
  // Should throw an exception when getting an unknown key
  try {
    cache.get("?");
    cout << "[KO]";
  } catch (out_of_range) {
    cout << "[OK]";
  }
  cout << " Should throw an exception when getting an unknown key" << endl;

  // Test 3
  // Should throw an exception when removing an unknown key
  try {
    cache.remove("?");
    cout << "[KO]";
  } catch (out_of_range) {
    cout << "[OK]";
  }
  cout << " Should throw an exception when removing an unknown key" << endl;

  // Test 4
  // Should add elements when the cache is not full
  try {
    cache.add("A", 1);
    cache.add("B", 2);
    cout << ((cache.get("A") == 1 && cache.get("B") == 2) ? "[OK]" : "[KO]");
  } catch (...) {
    cout << "[KO]";
  }
  cout << " Should add elements when the cache is not full" << endl;

  // Test 5
  // Should remove the element
  try {
    cache.add("A", 1);
    cache.remove("A");
    cache.get("A");
    cout << "[KO]";
  } catch (...) {
    cout << "[OK]";
  }
  cout << " Should remove the element" << endl;

  // Test 6
  // Should remove the oldest item when the cache is full
  try {
    cache.add("A", 1);
    cache.add("B", 2);
    cache.add("C", 3);
    cache.get("A");
  } catch (...) {
    cout << ((cache.get("B") == 2 && cache.get("C") == 3) ? "[OK]" : "[KO]");
  }
  cout << " Should remove the oldest item when the cache is full" << endl;

  // Test 7
  // Should reset the element's status when using get
  try {
    cache.add("A", 1);
    cache.add("B", 2);
    cache.get("A");
    cache.add("C", 3);
    cout << (cache.get("A") == 1 ? "[OK]" : "[KO]");
  } catch (...) {
    cout << "[KO]";
  }
  cout << " Should reset the element's status when using get" << endl;

  // Test 8
  // Should overwrite the element when using add
  try {
    cache.add("A", 1);
    cache.add("A", 2);
    cout << (cache.get("A") == 2 ? "[OK]" : "[KO]");
  } catch (...) {
    cout << "[KO]";
  }
  cout << " Should overwrite the element when using add" << endl;

  // Test 9
  // Should reset the element's status when using add
  try {
    cache.add("A", 1);
    cache.add("B", 2);
    cache.add("A", 4);
    cache.add("C", 3);
    cache.get("B");
  } catch (...) {
    cout << ((cache.get("A") == 4 && cache.get("C") == 3) ? "[OK]" : "[KO]");
  }
  cout << " Should reset the element's status when using add" << endl;

  return 0;
}